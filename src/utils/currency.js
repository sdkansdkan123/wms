export const formatCurrency = function(value){
  value = Math.ceil(value);
  let val = (value/1).toFixed(0).replace('.', ',')
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " VNĐ"
}