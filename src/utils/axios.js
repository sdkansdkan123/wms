/**
 * HTTP client.
 *
 * @see {@link https://github.com/axios/axios}
 * @module utils/axios
 */
import _ from 'lodash'
import axios from 'axios'
import store from '../store'
import notification from '@/utils/notification'
import router from '../router/index'

const config = {
  baseURL: process.env.VUE_APP_API,
  timeout: 100000
}

const instance = axios.create(config)

instance.interceptors.request.use(
  function (config) {
    const accessToken = store.state.auth.user.access_token
    //const accessToken = "aaeyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0aGFuZ2NvaSIsInVzZXJfaWQiOjgsIm93bmVyX2lkIjoxLCJyb2xlcyI6WyJUZXN0XzAxIl0sImlzcyI6ImF1dGgwIiwiZXhwIjoxNjI2NTI4MjA2fQ.lNEj5aGCN1esbi3Vm6jXmG8q3X6WY5BARs8E1E-hPLc"
    if (!_.isEmpty(accessToken)) {
      config.headers.Authorization = `Bearer ${accessToken}`
    }
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  response => {

    if (response.data.code != 1) {
      notification.error(response.data.message)
      return Promise.reject(response)
    }
    return response
  },
  error => {
    // Check for errorHandle config
    //  if (error.config.hasOwnProperty('errorHandle') && error.config.errorHandle === false) {
    //    return Promise.reject(error)
    //  }

    // Handling error
    if (error.response) {
      if (error.response?.data?.message === "Token invalid") {
        store.dispatch('auth/logout').then(() => {
          router.push({ path: '/auth/login' })
        })
        return Promise.reject(error)
      }
      let message = ''
      if (error.response.data.message) { message += error.response.data.message + ' ' }
      if (error.response.data.title) { message += error.response.data.title + ' ' }
      if (error.response.data.modelState) { message += error.response.data.modelState.invalid_grant + ' ' }
      if (error.response.data.innerException) { message += error.response.data.innerException.exceptionMessage }

      notification.error(message);

      return Promise.reject(error)
    }
  }
)

// axios methods
const methods = {
  /**
     * get.
     * @param {String} url Url
     * @param {Object} config Config
     * @return {Promise} Promise
     * @see {@link https://github.com/mzabriskie/axios#instance-methods}
     */
  get(url, config) {
    return instance.get(url, config)
  },

  /**
     * delete.
     * @param {String} url Url
     * @param {Object} config Config
     * @return {Promise} Promise
     * @see {@link https://github.com/mzabriskie/axios#instance-methods}
     */
  delete(url, config) {
    return instance.delete(url, config)
  },


  /**
     * post.
     * @param {String} url Url
     * @param {Object} data Data
     * @param {Object} config Config
     * @return {Promise} Promise
     * @see {@link https://github.com/mzabriskie/axios#instance-methods}
     */
  post(url, data, config) {
    return instance.post(url, data, config)
  },

  /**
     * put.
     * @param {String} url Url
     * @param {Object} data Data
     * @param {Object} config Config
     * @return {Promise} Promise
     * @see {@link https://github.com/mzabriskie/axios#instance-methods}
     */
  put(url, data, config) {
    return instance.put(url, data, config)
  }
}

export default methods
