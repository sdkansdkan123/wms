import { fireEvent } from '@/utils/event'
import { ToastEvent } from "@/models/enums";

export default {
    success(message){
        fireEvent(ToastEvent.SHOW_SUCCESS, message)
    },
    error(message, title = null){
        fireEvent(ToastEvent.SHOW_ERROR, message, title)
    }
}