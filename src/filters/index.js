import moment from 'moment'

export function formatCurrency (value){
    value = Math.ceil(value);
    let val = (value/1).toFixed(0).replace('.', ',')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " VNĐ"
}
export function formatOrderStatus(value){
    switch(value){
        case 0: return 'Đang xử lý'
        case 1: return 'Đã duyệt'
        default: return 'Unknow'
    }
}

export function formatDate(date){
    if(!date)
      return "";
    return moment(date).format("DD/MM/YYYY")
  }