import http from '@/utils/axios'

export default {
    getFunctions(){
        return http.get(`/function`).then(rs => rs.data.data.items)
    },
    createFunction(data){
        return http.post(`/function`, data)
    },
    updateFunction(data){
        return http.put(`/function`, data);
    }
}