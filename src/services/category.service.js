import http from '@/utils/axios'

export default {
    getCategories(page, pageSize){
        return http.get(`/category?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    addCategory(data){
        return http.post('/category', data).then(rs => rs.data)
    },
    updateCategory(data){
        return http.put('/category', data).then(rs => rs.data)
    },
    deleteCategory(id) {
        return http.delete(`/category/${id}`).then(rs => rs.data)
    }
}