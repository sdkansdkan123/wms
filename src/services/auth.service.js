import http from '@/utils/axios'

export default {
    login(data){
        return http.post(`/login`, data).then(rs => rs.data.data).catch(rs=>rs.response.data);
    },
    signUp(data){
        return http.post(`/public/signup`, data).then(rs =>rs.data.data ).catch(rs=>rs.response.data);
    },
    forgotPassWord(data){
        return http.post(`/public/forgot-password`, data).then(rs =>rs.data);
    },
    resetPassWord(data){
        return http.post(`/public/reset-password`, data).then(rs =>rs.data);
    },
    getUserInfo(){
        return http.get('/me').then(rs => rs.data.data)
    }
}