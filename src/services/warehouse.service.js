import http from '@/utils/axios'

export default {
    getWarehouses(){
        return http.get(`/warehouse`).then(rs => rs.data.data.items)
    },
    getWarehouseDetails(warehouse_id){
        return http.get(`/warehouse/${warehouse_id}`).then(rs => rs.data.data)
    },
    addWarehouse(data){
        return http.post(`/warehouse`,data).then(rs => rs.data.code)
    },
    updateWarehouse(data){
        return http.put(`/warehouse`,data).then(rs => rs.data.code)
    },
    getListItems(warehouse_id,page,pageSize){
        return http.get(`/warehouse/${warehouse_id}/items?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    searchWarehouse(dataSearch){
        return http.get(`/search-warehouse?q=${dataSearch}`);
    }
}