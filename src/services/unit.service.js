import http from '@/utils/axios'

export default {
    getUnits(page, pageSize){
        return http.get(`/unit?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    addUnit(data){
        return http.post('/unit', data).then(rs => rs.data)
    },
    updateUnit(data){
        return http.put('/unit', data).then(rs => rs.data)
    },
    deleteUnit(id) {
        return http.delete(`/unit/${id}`).then(rs => rs.data)
    }
    
}