import http from '@/utils/axios'

export default {
    getBrands(page, pageSize) {
        return http.get(`/brand?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    updateBrand(data) {
        return http.put(`/brand`, data).then(rs => rs.data)
    },
    deleteBrand(id) {
        return http.delete(`/brand/${id}`).then(rs => rs.data)
    },
    addBrand(data) {
        return http.post(`/brand`, data).then(rs => rs.data)
    },
    brandDetail(id) {
        return http.get(`/brand/${id}`).then(rs =>{return rs.data.data} )
    }

}