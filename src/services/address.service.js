import http from '@/utils/axios'

export default {
    getProvinces(){
        // return null
        return http.get(`/d/province`).then(rs => { return rs.data.data.items })
    },
    getDistricts(province_id){
        if(province_id===null) return []
        return http.get(`/d/district?province_id=${province_id}`).then(rs => {  
            return rs.data.code != 500 ? rs.data.data.items : []
        })
    },
    getWards(district_id){
        console.log('ward',district_id);
        if(district_id===null) return []
        return http.get(`/d/ward?district_id=${district_id}`).then(rs => {
            return rs.data.code != 500 ? rs.data.data.items : []
        })
    }
}