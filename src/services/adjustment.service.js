import http from '@/utils/axios'

export default {
    getAdjustments(page, pageSize){
        return http.get(`/adjustment?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    addAdjustment(data){
        return http.post(`/adjustment`, data)   
    },
    deleteAdjustment(id){
        return http.delete(`/adjustment/${id}`)   
    },
    updateAdjustment(data){
        return http.put(`/adjustment`, data)
    },
    searchItemAdjustment(warehouseId, dataSearch){
        return http.get(`/adjustment/search-item?warehouse_id=${warehouseId}&q=${dataSearch}`);
    },
    getAdjustmentItems(id){
        return http.get(`/adjustment/${id}/item`).then(rs => rs.data.data)
    },
    updateAdjustmentItem(id, item){
        return http.put(`/adjustment/${id}/item`, item)
    },
    deleteAdjustmentItem(id, itemId){
        return http.delete(`/adjustment/${id}/item/${itemId}`)
    },
    addAdjustmentItem(id, item){
        return http.post(`/adjustment/${id}/item`, item)
    }
}