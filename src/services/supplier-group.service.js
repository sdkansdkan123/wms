import http from '@/utils/axios'

export default {
    getAllSupplierGroup(){
        return http.get(`/supplier-group`).then(rs => {return rs.data.data.items})
    },
    getSupplierGroups(page,pageSize){
        return http.get(`/supplier-group?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    }
}