import http from '@/utils/axios'

export default {
    searchItemTransfer(warehouseId, dataSearch){
        return http.get(`/transfer/search-item?warehouse_id=${warehouseId}&q=${dataSearch}`)
    },
    getTransfers(page, pageSize){
        return http.get(`/transfer?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    getAllTransfer(){
        return http.get(`/transfer`).then(rs => rs.data.data.items)
    },
    getAllItem(transferId){
        return http.get(`/transfer/${transferId}/item`).then(rs => rs.data.data?rs.data.data:[])
    },
    getItems(transferId,page,pageSize){
        return http.get(`/transfer/${transferId}/item?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data?rs.data.data:[])
    },
    addTransfer(data){
        return http.post(`/transfer`, data).then(rs => rs.data.code)
    },
    updateTransfer(data){
        return http.put(`/transfer`, data).then(rs => rs.data.code)
    },
    deleteTransfer(transfer_id){
        return http.delete(`/transfer/${transfer_id}`).then(rs => rs.data.code)
    },
    addItem(id,data){
        return http.post(`/transfer/${id}/item`,data).then(rs => rs.data.code)
    },
    updateItem(id,data){
        return http.put(`/transfer/${id}/item`,data).then(rs => {rs.data.code})
    },
    deleteItem(id,vid){
        return http.delete(`/transfer/${id}/item/${vid}`).then(rs => rs.data)
    },
}