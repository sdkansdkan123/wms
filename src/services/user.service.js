import http from '@/utils/axios'

export default {
    getUsers(page, pageSize) {
        return http.get(`/user?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    getRoleGroup(id) {
        if (id===null) return null
        return http.get(`/user/${id}/group`).then(rs => rs.data.data.items)
    },
    createUser(data) {
        return http.post(`/user`, data).then(rs => rs.data);
    },
    updateUser(data) {
        return http.put(`/user`, data).then(rs => rs.data);
    },
    addToGroup(id, data) {
        return http.post(`/user/${id}/add-to-group`, data).then(rs => rs.data.message)
    },
    delFromGroup(id, data) {
        return http.post(`/user/${id}/del-from-group`, data).then(rs => rs.data.message)
    },
    deleteUser(id) {
        return http.delete(`/user/${id}`)
    }
}