import http from '@/utils/axios'

export default {
    searchItemPurchase(warehouseId, dataSearch){
        return http.get(`/purchase/search-item?warehouse_id=${warehouseId}&q=${dataSearch}`);
    },
    // Purchase
    getPurchaseListByPage(page, pageSize){ 
        return http.get(`/purchase?page=${page}&pageSize=${pageSize}`).then(rs =>rs.data.data)
    },
    getPurchases(){ 
        return http.get(`/purchase`).then(rs => rs.data.data.items)
    },
    getPurchaseDetail(id){
        return http.get(`/purchase/${id}/details`).then(rs => rs.data.data)
    },
    addPurchase(data){
        return http.post(`/purchase`,data).then(rs => rs.data)
    },
    updatePurchase(data){
        return http.put(`/purchase`,data).then(rs => rs.data)
    },
    deletePurchase(id){
        return http.delete(`/purchase/${id}`).then(rs => rs.data)
    },
    approvedPurchase(id){
        return http.put(`/purchase/${id}/approved`).then(rs => rs.data)
    },
    // Order list
    getOrderItemList(id){
        return http.get(`/purchase/${id}/item`).then(rs => rs.data.data.items)
    },
    addOrderItem(id,data){
        return http.post(`/purchase/${id}/item`, data).then(rs => rs.data.data)
    },
    updateOrderItem(id, data){
        return http.put(`/purchase/${id}/item`, data).then(rs => rs.data.data)
    },
    deleteOrderItem(id, vid){
        return http.delete(`/purchase/${id}/item/${vid}`).then(rs => rs.data)
    },
}