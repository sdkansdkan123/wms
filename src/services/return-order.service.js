import http from '@/utils/axios'

export default {
    getReturnOrder(page,pageSize){
        return http.get(`/return-order?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    addReturnOrder(data){
        return http.post(`/return-order`, data).then(rs => rs.data)
    },
    updateReturnOrder(data){
        return http.put(`/return-order`, data).then(rs => rs.data)
    },
    deleteReturnOrder(id){
        return http.delete(`/return-order/${id}`).then(rs => rs.data)
    },
    deleteReturnOrderItems(id,vid){
        return http.delete(`/return-order/${id}/item/${vid}`).then(rs => rs.data);
    },
    getItemReturnOrders(id,page,pageSize){
        return http.get(`/return-order/${id}/item?page=${page}&pageSize=${pageSize}`).then(rs => rs.data)
    },
    getItemsReturnOrders(id){
        return http.get(`/return-order/${id}/item?page=0&pageSize=99999`).then(rs => rs.data)
    },
    updateItemsReturnOrders(id,data){
        return http.put(`/return-order/${id}/item`, data).then(rs => rs.data)
    },
    getItemSaleOrders(id,page,pageSize){
        return http.get(`/sale-order/${id}/item?page=${page}&pageSize=${pageSize}`).then(rs => rs.data)
    },
    getItemsSaleOrders(id){
        return http.get(`/sale-order/${id}/item?page=0&pageSize=99999`).then(rs => rs.data)
    },
    getReturnOrderDetails(id){
        return http.get(`/return-order/${id}/details`).then(rs => rs.data|| [])
    },
    getSaleOrderDetails(id){
        return http.get(`/sale-order/${id}/details`).then(rs => rs?.data || [])
    },
}