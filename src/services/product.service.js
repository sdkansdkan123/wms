import http from '@/utils/axios'

export default {
    getProducts(page, pageSize, type){
        return http.get(`/product?page=${page}&pageSize=${pageSize}&type=${type}`).then(rs => rs.data.data)
    },
    getAllProducts(){
        return http.get(`/product`).then(rs => rs.data.data)
    },
    addProduct(data){
        return http.post(`/product`, data)
    },
    updateProduct(data){
        return http.put(`/product`, data)
    },
    getProductById(id){
        return http.get(`/product/${id}`).then(rs=>rs.data.data)
    },
    deleteProduct(id){
        return http.delete(`/product/${id}`).then(rs=>rs.data)
    },
    getVariants(id){
        return http.get(`/product/${id}/variant`).then(rs=>rs.data.data)
    },
    putVariant(id, data){
        return http.put(`/product/${id}/variant`, data)
    },
    getWarehouses(id){
        return http.get(`/product/${id}/warehouse`).then(rs=>rs.data.data)
    },
    putWarehouse(id, data){
        return http.put(`/product/${id}/warehouse`, data)
    },
    getUnits(id){
        return http.get(`/product/${id}/unit`).then(rs=>rs.data.data)
    },
    putUnit(id, data){
        return http.put(`/product/${id}/unit`, data)
    },
    getOthers(id){
        return http.get(`/product/${id}/other`).then(rs=>rs.data.data)
    },
    putOther(id, data){
        return http.put(`/product/${id}/other`, data)
    },
    putPrice(id, data){
        return http.put(`/product/${id}/price`, data)
    }
}