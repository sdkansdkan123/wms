import http from '@/utils/axios'

export default {
    getCustomers(){
        return http.get(`/customer-group`).then(rs => rs.data.data.items)
    },
    createCustomer(data){
        return http.post(`/customer-group`, data).then(rs => rs.data);
    },
    updateCustomer(data){
        return http.put(`/customer-group`, data).then(rs => rs.data);
    }
}