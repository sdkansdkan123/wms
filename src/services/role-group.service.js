import http from '@/utils/axios'

export default {
    getRoleGroups(page, pageSize){
        return http.get(`/role-group?page=${page}&pageSize=${pageSize}`).then(rs => rs)
    },
    addRoleGroups(data){
        return http.post(`/role-group`,data).then(rs=>rs.data)
    },
    updateRoleGroups(data){
        return http.put(`/role-group`,data).then(rs=>rs.data)
    },
    deleteRoleGroups(data){
        return http.delete(`/role-group/${data}`).then(rs=>rs)
    },
    getRoleGroupFunctions(groupId){
        return http.get(`/role-group/${groupId}/function`).then(rs=> rs.data)
    },
    addRemoveGroupFunctions(groupId,data){
        return http.post(`/role-group/${groupId}/function`,data).then(rs=> rs.data)
    },
    getRoleGroupPermissions(groupId){
        return http.get(`/role-group/${groupId}/permission`).then(rs=> rs.data)
    }
}