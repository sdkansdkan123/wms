import http from '@/utils/axios'

export default {
    getSupplier(){
        return http.get(`/supplier`).then(rs => rs.data.data.items)
    },
    getSuppliers(page,pageSize){
        return http.get(`/supplier?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    addSupplier(data){
        return http.post(`/supplier`, data).then(rs => rs.data)
    },
    updateSupplier(data){
        return http.put(`/supplier`,data).then(rs => rs.data.code)
    },
    deleteSupplier(id){
        return http.delete(`/supplier/${id}`).then(rs => rs.data.code)
    },
    getSupplierById(id){
        return http.get(`/supplier/${id}`).then(rs => rs.data.data)
    },
    searchSupplier(dataSearch){
        return http.get(`/supplier?page=0&pageSize=1000&q=${dataSearch}`)
    },
    getListAllSupplierAddress(id){
        return http.get(`/supplier/${id}/address`).then(rs => rs.data.data.items)
    },
    getSupplierAddress(id,page,pageSize){
        return http.get(`/supplier/${id}/address?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    deleteSupplierAddress(id,aid){
        return http.delete(`/supplier/${id}/address/${aid}`).then(rs => rs.data.code)
    },
    updateSupplierAddress(id,data){
        return http.put(`/supplier/${id}/address`, data).then(rs => rs.data.code)
    },
    addSupplierAddress(id,data){
        return http.post(`/supplier/${id}/address`, data).then(rs => rs.data.code)
    },
}