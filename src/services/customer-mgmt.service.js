
import http from '@/utils/axios'
export default {
    //get
    getCustomerList(page, pageSize){
        return http.get(`/customer?page=${page}&pageSize=${pageSize}`).then(rs => rs)
    },
    getCustomerById(id){
        if(id==null) return null
        return http.get(`/customer/${id}`).then(rs => rs)
    },
    getCustomerGroupList(page, pageSize){
        if(!page || !pageSize){
            return http.get(`/customer-group`).then(rs => rs)
        }
        return http.get(`/customer-group?page=${page}&pageSize=${pageSize}`).then(rs => rs)
    },
    getCustomerContacts(id){
        return http.get(`/customer/${id}/contact?page=0&pageSize=1000&`).then(rs => rs)
    },
    getCustomerAddresses(cid){
        return http.get(`/customer/${cid}/address`).then(rs => rs)
    },
    searchCustomer(dataSearch){
        return http.get(`/customer?page=0&pageSize=1000&q=${dataSearch}`)
    },

    //post
    addCustomerGroup(data){
        return http.post(`/customer-group`, data).then(rs =>rs.data ).catch(rs=>rs);
    },
    addCustomer(data){
        return http.post(`/customer`, data).then(rs =>rs.data);
    },
    addCustomerContact(id,data){
        return http.post(`/customer/${id}/contact`, data).then(rs =>rs.data ).catch(rs=>rs);
    },
    addCustomerAddress(cid,data){
        return http.post(`/customer/${cid}/address`, data).then(rs =>rs.data ).catch(rs=>rs);
    },

    //put
    updateCustomerGroup(data){
        return http.put(`/customer-group`, data).then(rs =>rs.data ).catch(rs=>rs.data);
    },
    updateCustomer(data){
        return http.put(`/customer`, data).then(rs =>rs.data ).catch(rs=>rs.data);
    },
    updateCustomerContact(id, data){
        return http.put(`/customer/${id}/contact`, data).then(rs =>rs.data ).catch(rs=>rs.data);
    },
    updateCustomerAddress(id, data){
        return http.put(`/customer/${id}/address`, data).then(rs =>rs.data ).catch(rs=>rs.data);
    },

    //delete
    deleteCustomerGroup(id){
        return http.delete(`/customer-group/${id}`).then(rs =>rs.data ).catch(rs=>rs);
    },
    deleteCustomer(id){
        return http.delete(`/customer/${id}`).then(rs =>rs.data ).catch(rs=>rs);
    },
    deleteCustomerContact(cid, id){
        return http.delete(`/customer/${cid}/contact/${id}`).then(rs =>rs.data ).catch(rs=>rs);
    },
    deleteCustomerAddress(cid, id){
        return http.delete(`/customer/${cid}/address/${id}`).then(rs =>rs.data ).catch(rs=>rs);
    }
} 