import http from '@/utils/axios'

export default {
    upload(files){
        const formData = new FormData();
        for (let i = 0; i < files.length; i++) {
            const file = files[i];
            formData.append('files', file);
        }
        return http.post(`/upload`, formData).then(rs => rs.data.data.items);
    }
}