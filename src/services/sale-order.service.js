import http from '@/utils/axios'

export default {
    getSaleOrder(page, pageSize) {
        return http.get(`/sale-order?page=${page}&pageSize=${pageSize}`).then(rs => rs.data.data)
    },
    createSaleOrder(data) {
        console.log('a',data);
        // var rs = {
        //     code: "mdh09111",
        //     ref: "mdh0911111112",
        //     status: 0,
        //     tags: ["a"
        //     ],
        //     customer_id: 89,
        //     warehouse_id: 7,
        //     payment_type: 1,
        //     pay: 2000,
        //     order_items: [
        //         {
        //             variant_id: 92,
        //             number: 1,
        //             price: 10000,
        //             discount: 0,
        //             tax: 0
        //         }
        //     ]
        // }

        return http.post(`/sale-order`, data).then(rs => rs.data).catch(rs => rs.data);
    },
    updateSaleOrder(data) {
        return http.put(`/sale-order`, data).then(rs => rs.data).catch(rs => rs.data);
    },
    deleteSaleOrder(id) {
        return http.delete(`/sale-order/${id}`).then(rs => rs.data).catch(rs => rs.data);
    },
    searchItemSaleOrder(warehouseId, dataSearch) {
        return http.get(`/sale-order/search-item?warehouse_id=${warehouseId}&q=${dataSearch}`);
    },
    getOrderItems(id) {
        return http.get(`/sale-order/${id}/item`).then(rs => rs.data.data.items).catch(rs => rs.data);
    },
    updateItem(id, data) {
        return http.put(`/sale-order/${id}/item`, data).then(rs => { return rs.data }).catch(rs => rs.data);
    },
    addItem(id, data) {
        return http.post(`/sale-order/${id}/item`, data).then(rs => { return rs.data }).catch(rs => rs.data);
    },
    deleteItem(id, vid) {
        return http.delete(`/sale-order/${id}/item/${vid}`).then(rs => rs.data).catch(rs => rs.data);
    },
    getSaleOrderDetail(id) {
        return http.get(`/sale-order/${id}/details`).then(rs => rs.data.data)
    },
}