import { is } from 'ramda'

export const ColumnType = {
  TEXT: 'text',
  IMAGE: 'image',
  NUMBER: 'number',
  DECIMAL: 'decimal',
  DATE: 'date_format',
  ACTION: 'action',
  BOOLEAN: 'boolean',
  CHECK_BOX: 'checkBox',
  INPUT_NUMBER: 'input_number',
  INPUT_CURRENCY: 'input_currency',
  LINE_NUMBER: 'line_number',
  CURRENCY: 'currency',
  ORDER_STATUS: 'order_status',
}

export class FilterOption {
  constructor(
    styleClass = '', // class to be added to the parent th element
    enabled = true, // enable filter for this column
    placeholder = '', // placeholder for filter input
    filterValue = '', // initial populated value for this filter
    filterDropdownItems = [], // dropdown (with selected values) instead of text input
    // filterFn = () => {}, //custom filter function that
    trigger = 'enter', //only trigger on enter not on keyup 
  ){
    this.styleClass = styleClass;
    this.enabled=enabled;
    this.placeholder = placeholder;
    this.filterValue = filterValue;
    this.filterDropdownItems = filterDropdownItems;
    // this.filterFn = filterFn;
    this.trigger = trigger;
  }
  


}

export class ColumnTable {
  constructor (type, label, field, option) {
    this.type = type;
    this.label = is(String, label) ? label : '';
    this.field = is(String, field) ? field : '';

    this.sortable = is(Boolean, option?.sortable) ? option.sortable : false;
    this.actions = is(Array, option?.actions) ? option.actions: null;
    this.firstSortType = is(String, option?.firstSortType)? option.firstSortType: '';
    this.filterOptions =  is(FilterOption, option?.filterOptions)? option.filterOptions: null;
    this.width =  is(String, option?.width)? option.width: null;
    this.thClass =  is(String, option?.thClass)? option.thClass: null;
    this.tdClass =  is(String, option?.tdClass)? option.tdClass: null;
    this.disabled =  is(Boolean, option?.disabled)? option.disabled: false;
    this.minValue =  is(Number, option?.minValue)? option.minValue: 0;
    this.maxValue =  is(Number, option?.maxValue)? option.maxValue: 9999999999999;
  }
}

export class Action {
  constructor (icon, func, className = '', option, prop, iconOther) {
    this.icon = is(String, icon) ? icon : '';
    this.func = is(Function, func) ? func : null;
    this.className = className;
    this.isDisableByStatus = is(Function, option?.isDisableByStatus)? option.isDisableByStatus: () =>{ return false };
    this.prop = is(String, prop) ? prop : '';
    this.iconOther = is(String, iconOther) ? iconOther : '';
  }
}