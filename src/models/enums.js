export const DialogName = {
    GROUP_MANAGEMENT:'group-mgmt',
    GROUP_ADD_EDIT:'group-add-edit',
    COMMON_DIALOG:'common-dialog',
    ADJUSTMENT_ADD_EDIT: 'adjustment-add-edit',
    BRAND_MANAGEMENT: 'brand-mgmt',
    BRAND_ADD_EDIT: 'brand-add-edit-mgmt'
}
export const ToastEvent = {
    SHOW_ERROR:'SHOW_ERROR',
    SHOW_SUCCESS:'SHOW_SUCCESS'
}
export const ButtonEvent = {
   BTN_CLOSE:'close-button',
   BTN_SAVE:'save-button',
   BTN_UPDATE:'update-button'
}
export const SEARCH = {
    CUSTOMER:'customer',
    WAREHOUSE:'warehouse',
    PAYMENT:'payment'
 }

export const TypeSearch = {
    PURCHASE: 'purchase',
    TRANSFER: 'transfer',
    ADJUSTMENT: 'adjustment'
}

export const PAGE = {
    SIZE: 10,
}

