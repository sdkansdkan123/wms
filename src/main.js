import '@babel/polyfill'
import 'mutationobserver-shim'
import Raphael from 'raphael/raphael'
global.Raphael = Raphael
import Vue from 'vue'
import './plugins'
import App from './App.vue'
import router from './router'
import store from './store'
import './directives'
import * as VueGoogleMaps from "vue2-google-maps";
import { formatCurrency, formatOrderStatus, formatDate } from '@/filters'
import VueCurrencyInput from 'vue-currency-input'

import 'vue-tree-halower/dist/halower-tree.min.css'
import VTree from 'vue-tree-halower'

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCZNA0nJO06usfVWObeE2PFYQYOjJExbhI"
  }
});
Vue.use(VueCurrencyInput)
Vue.filter('currency', formatCurrency)
Vue.filter('orderStatus', formatOrderStatus)
Vue.filter('dateFormat', formatDate)
Vue.use(VTree)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
