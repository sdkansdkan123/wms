import Vue from 'vue'

Vue.component('card', require('../components/cards/card').default)
Vue.component('Progressbar', require('../components/progressbar/Progressbar').default)
Vue.component('Slick', require('../components/slider/Slick').default)
Vue.component('tab-nav', require('../components/tab/tab-nav').default)
Vue.component('tab-nav-items', require('../components/tab/tab-nav-items').default)
Vue.component('tab-content', require('../components/tab/tab-content').default)
Vue.component('tab-content-item', require('../components/tab/tab-content-item').default)
Vue.component('ModeSwitch', require('../components/mode/ModeSwitch').default)
Vue.component('Select2', require('../components/form/select2/Select2').default)
Vue.component('VueSelectPicker', require('../components/form/selectpicker/VueSelectpicker').default)
Vue.component('TableList', require('../components/table/tablelist').default)
Vue.component('titlesectionList', require('../components/titlesection/titlesectionlist').default)

Vue.component('Dialog', require('../components/dialog/Dialog').default)
Vue.component('Dropdown', require('../components/dropdown/Dropdown').default)
Vue.component('Nav-tab', require('../components/navtab/NavTab').default)
Vue.component('Nav-tab-item', require('../components/navtab/NavTab-item').default)

//Vue.component('b-icon', require('bootstrap-vue'))

import VueGoodTablePlugin from 'vue-good-table';

Vue.use(VueGoodTablePlugin);