import Address from '@/services/address.service';

export default {
    namespaced: true,
    state: {
        provinces: [],
        count: 0,
        currentProvinceId: null,
        currentDistrictId: null,
        currentWardId: null,
        districts: [],
        wards: [],
        newProvinces: [],
        newDistricts: [],
        newWards: []
    },
    actions: {
        async getProvinces({ commit }) {
            const result = await Address.getProvinces()
            if (result) {
                commit('setProvinces', result)
                return result
            }
            return null
        },
        async getDistricts({ commit }, province_id) {
            const result = await Address.getDistricts(province_id)
            if (result) {
                commit('setDistricts', result)
                return result
            }
            return null
        },
        async getWards({ commit }, district_id) {
            const result = await Address.getWards(district_id)
            if (result) {
                commit('setWards', result)
                return result
            }
            return null
        },
        async getDistrictsAddEdit({ commit }, data) {
            const result = await Address.getDistricts(data.province_id)
            if (result) {
                console.log('getDistrictsAddEdit', result);
                data.isUpdate ? commit('setDistricts', result) : commit('setDistrictsAdd', result)
                return result
            }
            return null
        },
        async getWardsAddEdit({ commit }, data) {
            const result = await Address.getWards(data.ward_id)
            if (result) {
                data.isUpdate ? commit('setWards', result) : commit('setWardsAdd', result)

                return result
            }
            return null
        },
        setProvinces({ commit }, provinces) {
            commit('setProvinces', provinces);
        },
        setDistricts({ commit }, districts) {
            commit('setDistricts', districts);
        },
        setWards({ commit }, wards) {
            commit('setWards', wards);
        },
        setDistrictsAdd({ commit }, districts) {
            commit('setDistrictsAdd', districts);
        },
        setWardsAdd({ commit }, wards) {
            var rs = wards === '0' ? [] : wards
            commit('setWardsAdd', rs);
        },
        setCurrentProvince({ commit }, province_id) {
            commit('setCurrentProvinceId', province_id)
        },
        setCurrentDistrict({ commit }, district_id) {
            commit('setCurrentDistrictId', district_id)
        },
        setCurrentWard({ commit }, ward_id) {
            commit('setCurrentWardId', ward_id)
        },
    },
    getters: {
        provinces: (state) => {
            return state.provinces
        },
        currentProvince: (state) => {
            return state.currentProvinceId
        },
        districts: (state) => {
            return state.districts
        },
        currentProvinceAdd: (state) => {
            return state.currentProvinceId
        },
        districtsAdd: (state) => {
            return state.newDistricts
        },
        currentDistrict: (state) => {
            return state.currentDistrictId
        },
        wards: (state) => {
            return state.wards
        },
        wardsAdd: (state) => {
            return state.newWards
        },
        currentWard: (state) => {
            return state.currentWardId
        }
    },
    mutations: {
        setProvinces: (state, provinces) => {
            state.provinces = provinces
        },
        setCurrentProvinceId: (state, currentProvinceId) => {
            state.currentProvinceId = currentProvinceId
        },
        setDistricts: (state, district) => {
            state.districts = district
        },
        setDistrictsAdd: (state, district) => {
            state.newDistricts = district
        },
        setCurrentDistrictId: (state, district_id) => {
            state.currentDistrictId = district_id
        },
        setWardsAdd: (state, wards) => {
            state.newWards = wards
        },
        setWards: (state, wards) => {
            state.wards = wards
        },
        setCurrentWardId: (state, ward_id) => {
            state.currentWardId = ward_id
        }
    }
}