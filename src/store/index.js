import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
addressModule
import authModule from './auth';
import addressModule from './address';
import functionModule from './function'
import productModule from './product'
import unitModule from './unit'
import warehouseModule from './warehouse'
import categoryModule from './category'
import brandModule from './brand'
import roleGroupModule from './role-group'
import customerGroupModule from './customer-group'
import userModule from './user'
import queueDialog from './queue-dialog';
import customerManagementModule from './customer-mgmt'
import adjustmentModule from './adjustment'
import transferModule from './transfer'
import supplierModule from './supplier'
import purchaseModule from './purchase'
import saleOrder from './sale-order'
import supplierGroupModule from './supplier-group';
import returnOrderModule from './return-order'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    appName: ' POS Dash',
    logo: require('../assets/images/logo.png'),
    darklogo: require('../assets/images/logo.png'),
    dark: false,
    namespaced: true,
    user: {
      name: 'Bill Yerds',
      image: require('../assets/images/user/1.jpg'),
    }
  },
  mutations: {
    layoutModeCommit(state, payload) {
      state.dark = payload
      if (!payload) {
        state.logo = require('../assets/images/logo.png')
      } else {
        state.logo = require('../assets/images/logo.png')
      }
    },
    setRoleGroups: (state, roleGroups) => {
      state.roleGroups = roleGroups
    }
  },
  actions: {
    layoutModeAction(context, payload) {
      context.commit('layoutModeCommit', payload.dark)
    }
  },
  getters: {
    appName: state => { return state.appName },
    logo: state => { return state.logo },
    darklogo: state => { return state.darklogo },
    image1: state => { return state.user.image },
    name: state => { return state.user.name },
    image2: state => { return state.user.image2 },
    image3: state => { return state.user.image3 },
    dark: state => { return state.dark },
  },
  modules: {
    auth: authModule,
    address: addressModule,
    function: functionModule,
    product: productModule,
    unit: unitModule,
    warehouse: warehouseModule,
    category: categoryModule,
    brand: brandModule,
    roleGroup: roleGroupModule,
    customerGroup: customerGroupModule,
    user: userModule,
    queueDialog:queueDialog,
    customerMgmt: customerManagementModule,
    adjustment: adjustmentModule,
    saleOrder: saleOrder,
    transfer: transferModule,
    supplier: supplierModule,
    purchase: purchaseModule,
    supplierGroup: supplierGroupModule,
    returnOrder: returnOrderModule
  },
  plugins: [createPersistedState()]
})
