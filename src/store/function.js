import functionService from '../services/function.service';

export default {
    namespaced: true,
    state: {
        selectFunctionNull: {},
        selectFunction: {},
        functions: [],
        functionCreated: {},
    },
    actions: {
        async loadFunctions({ commit }) {
            const result = await functionService.getFunctions()
            
            if (result) {
                commit('setFunctions', result)
                return JSON.parse(JSON.stringify(result))
            }
            return null
        },
        async createFunctions({ dispatch }, data) {
            const result = await functionService.createFunction(data)
            if (result) {
                //commit('setFunction', result)
                dispatch('loadFunctions')
                return result
            }
            return null
        },
        async updateFunction({ dispatch }, data) {
            const result = await functionService.updateFunction(data)
            if (result) {
                dispatch('loadFunctions')
                return result
            }
            return null
        },
        setSelectFunction({ commit }, selectFunction) {
            commit('setSelectFunction', selectFunction)
        }
    },
    getters: {
        getFunctions: (state) => {
            return state.functions
        },
        getFunction: (state) => {
            return state.functionCreated
        },
        getSelectFunction: (state) => {
            return state.selectFunction
        },
        getSelectFunctionNull: (state) => {
            return state.selectFunctionNull
        }
    },
    mutations: {
        setFunctions: (state, functions) => {
         //   console.log('SET_FUNCTON', functions)
            state.functions = JSON.parse(JSON.stringify(functions))
        },
        setFunction: (state, functionCreated) => {
            state.functionCreated = functionCreated
        },
        setSelectFunction: (state, selectFunction) => {
            state.selectFunction = selectFunction
        },
      
    }
}