import categoryService from '../services/category.service';

export default {
    namespaced: true,
    state: {
        categories: [],
        allCategories: [],
    },
    actions: {
        async loadCategories({ commit }, data) {
         
            const result = await categoryService.getCategories(data.page, data.pageSize)
            if(result){
                commit('setCategories', result)
                return result
            }
            return null
        },
        async loadAllCategories({ commit }) {
            const result = await categoryService.getCategories(0, 1000)
            if(result){
                commit('setAllCategories', result)
                return result
            }
            return null
        },
        async addCategory({dispatch}, data) {
            const rs = await categoryService.addCategory(data)

            if(rs){
                dispatch('loadCategories', {page:0, pageSize:100})
                return rs
            }

            return null
        },
        async updateCategory({ commit }, data) {
            const rs = await categoryService.updateCategory(data)

            if(rs) {
                commit('updateCategorySuccess', data)
                return rs
            }

            return null
        },
        async deleteCategory({ commit }, id) {
            const rs = await categoryService.deleteCategory(id)

            if(rs) {
                commit('deleteCategorySuccess', id)
                return rs
            }

            return null
        }
    
    },
    getters: {
        getCategories: (state) => {
            return state.categories
        },
        getAllCategories: (state) => {
            return state.allCategories
        }
    },
    mutations: {
        setCategories: (state, categories) => {
            state.categories = categories
        },
        setAllCategories: (state, categories) => {
            state.allCategories = categories
        },
        updateCategorySuccess: (state, category) => {
            state.categories.items = state.categories.items.map(r => {
                if (r.id === category.id) {
                    return category
                }
                else {
                    return r
                }
            })  
        },
        deleteCategorySuccess: (state, id) => {
            state.categories.items =  state.categories.items.filter(a => a.id !== id)
        }
    }
}