import warehouseService from '../services/warehouse.service';

export default {
    namespaced: true,
    state: {
        warehouse: [],
        currentWarehouse: {},
        listItemsOfWarehouse:[]
    },
    actions: {
        async getWarehouses({ commit }) {
            const result = await warehouseService.getWarehouses()
            if(result){
                commit('setWarehouses', result)
                return result
            }
            return null
        },
        async getWarehouseDetails({commit}, warehouse_id){
           
            const result = await warehouseService.getWarehouseDetails(warehouse_id)
            if(result){
                commit('setCurrentWarehouse', result)
                return result
            }
            return null
        },
        async addWarehouse({commit ,dispatch}, data){
            const result = await warehouseService.addWarehouse(data)
            if(result){
                commit('setCurrentWarehouse', data)
                dispatch('getWarehouses')
                return result
            }
            return null
        },
        async updateWarehouse({commit}, data){
            const result = await warehouseService.updateWarehouse(data)
            if(result){
                commit('setCurrentWarehouse', data)
                return result
            }
            return null
        },
        setCurrentWarehouse({commit}, warehouse){
            commit('setCurrentWarehouse', warehouse)
        },
        async getListItems({commit}, data){
            const result = await warehouseService.getListItems(data.id,data.page,data.pageSize)
            if(result){
                commit('setListItemsOfWarehouse', result)
                return result
            }
            return null
        }
    },
    getters: {
        warehouses: (state) => {
            return state.warehouse
        },
        currentWarehouse: (state) =>{
            return state.currentWarehouse
        },
        listItemsOfWarehouse: (state) =>{
            return state.listItemsOfWarehouse
        }
    },
    mutations: {
        setWarehouses: (state, warehouse) => {
            state.warehouse = warehouse
        },
        setCurrentWarehouse: (state,warehouse) =>{
            state.currentWarehouse = warehouse
        },
        setListItemsOfWarehouse: (state,listItems) =>{
            state.listItemsOfWarehouse = listItems
        }
    }
}