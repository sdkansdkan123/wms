import RoleGroupService from '../services/role-group.service'

export default {
    namespaced: true,
    state: {
        groupAddEdit:{
            code:'',
            roleName:'',
            description:'',
            isUpdate:'false',
        },
        roleGroups: null,
        count: 0,
        roleGroupFunctions: null,
        roleGroupPermissions: null,
        currentDialog: '',
        previousDialog: '',
        dialogQueue: []
    },
    actions: {
        async getRoleGroups({ commit }, data) {
            const result = await RoleGroupService.getRoleGroups(data.page, data.pageSize)
            if (result) {
              commit('setRoleGroups', result.data.data.items)
              return result.data.data
            }
            return null
          },
        async addRoleGroups({ commit }, data) {
            const result = await RoleGroupService.addRoleGroups(data)
            if (result) {
                commit('setRoleGroups', result);
                return result
            }
            return null
        },
        setRoleGroups({ commit }, roleGroups) {
            commit('setRoleGroups', roleGroups);
        },
        async updateRoleGroups({commit}, data) {
            const result = await RoleGroupService.updateRoleGroups(data)
            if (result) {
                commit('setRoleGroups', result);
                return result
            }
            return null
        },
        async deleteRoleGroups({commit}, data) {
            const result = await RoleGroupService.deleteRoleGroups(data)
            if (result) {
                commit('setRoleGroups', result);
                return result
            }
            return null
        },
        async getRoleGroupFunctions({ commit }, groupId){
            const rs = await  RoleGroupService.getRoleGroupFunctions(groupId)
            if(rs){
                commit('SET_ROLE_GROUPS_FUNCTIONS', rs.data)
            }
        },
        async getRoleGroupPermissions({ commit }, groupId){
            const rs = await  RoleGroupService.getRoleGroupPermissions(groupId)
            if(rs){
                commit('SET_ROLE_GROUPS_PERMISSIONS', rs.data)
            }
        },
        showDialog({commit}, dialogName){
            commit('pushToDialogQueue', dialogName);
        },
        hideDialog({commit}){
            commit('popDialogFromQueue')
        },
        onUpdateRoleGroup({commit},data){
            commit('setUpdateRoleGroup',data)
        }

    },
    getters: {
        roleGroups: (state) => {
            return state.roleGroups
        },
        roleGroupFunctions: (state) => {
            return state.roleGroupFunctions
        },
        roleGroupPermissions: (state) => {
            return state.roleGroupPermissions
        },
        currentDialog(state){
            return state.dialogQueue[state.dialogQueue.length - 1];
        },
        getGroupAddEdit:(state)=>{
            return  state.groupAddEdit;
        }
    },
    mutations: {
        setUpdateRoleGroup:(state,data)=>{
            state.groupAddEdit=data
        },
        setRoleGroups: (state, roleGroups) => {
            state.roleGroups = roleGroups
        },
        SET_ROLE_GROUPS_FUNCTIONS: (state, roleGroupFunctions) => {
            state.roleGroupFunctions = roleGroupFunctions
        },
        SET_ROLE_GROUPS_PERMISSIONS: (state, roleGroupPermissions) => {
            state.roleGroupPermissions = roleGroupPermissions
        },
        pushToDialogQueue(state, name){
            const exist = state.dialogQueue.find(d => d === name);
            if(!exist){
                state.dialogQueue = [...state.dialogQueue, name];
            }
        },
        popDialogFromQueue(state){
            if(state.dialogQueue.length > 0){
                state.dialogQueue.pop();
            }
        }
    }
}