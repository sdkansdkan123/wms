
export default {
    namespaced: true,
    state: {
        dialogQueue: []
    },
    actions: {
        showDialog({commit}, dialogName){
            commit('pushToDialogQueue', dialogName);
        },
        hideDialog({commit}){
            commit('popDialogFromQueue')
        }

    },
    getters: {
        currentDialog(state){
            return state.dialogQueue[state.dialogQueue.length - 1];
        }
    },
    mutations: {
        pushToDialogQueue(state, name){
            const exist = state.dialogQueue.find(d => d === name);
            if(!exist){
                state.dialogQueue = [...state.dialogQueue, name];
            }
        },
        popDialogFromQueue(state){
            if(state.dialogQueue.length > 0){
                state.dialogQueue.pop();
            }
        }
    }
}