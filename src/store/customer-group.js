import customerService from '../services/customer-group.service';

export default {
    namespaced: true,
    state: {
        customers: [],
        customersCreated: {}
    },
    actions: {
        async loadCustomers({ commit }) {
            const result = await customerService.getCustomers()
            if(result){
                commit('setCustomers', result)
                return result
            }
            return null
        },
        async createCustomer({ commit }, data){
            const result = await customerService.createCustomer(data)
            if (result) {
              commit('setCustomer', result)
              return result
            }
            return null
        },
        async updateCustomer({ commit }, data){
            const result = await customerService.updateCustomer(data)
            if (result) {
              commit('setCustomer', result)
              return result
            }
            return null
        }
    },
    getters: {
        getCustomers: (state) => {
            return state.customers
        },
        getCustomer: (state) => {
            return state.customersCreated
        }
    },
    mutations: {
        setCustomers: (state, customers) => {
            state.customers = customers
        },
        setCustomer: (state, customerCreated) => {
            state.customersCreated = customerCreated
        }
    }
}