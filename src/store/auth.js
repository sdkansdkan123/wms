import authService from '@/services/auth.service';

export default {
    namespaced: true,
    state: {
        user: { name: '', access_token: ''},
        count: 0
    },
    actions: {
        async signUp( { dispatch }, data) {
            const result = await authService.signUp(data)
            dispatch('');
            if (result) {
              return result
            }
            return null
          },
        async login({ commit }, data) {
            const result = await authService.login(data);
            if (result) {
              commit('setUserToken', result);
              const userInfo = await authService.getUserInfo();
              commit('setUserInfo', userInfo);
              return result
            }
            return null
          },
          setUser({commit}, user){
            commit('setUser', user);
        },
        logout({commit}){
          return new Promise((resolve) => {
            commit('removeUser');
            resolve();
          })
        }
    },
    getters: {
        getUser: (state) => {
            return state.user;
        }
    },
    mutations: {
        setUserToken: (state, user) => {
            state.user = user;
        },
        setUserInfo(state, userInfo){
          state.user = {...state.user, ...userInfo};
        },
        removeUser(state){
          state.user = {};
        }
    }
}