import supplierGroupService from '../services/supplier-group.service';


export default {
    namespaced: true,
    state: {
        supplierGroup: []
    },
    actions: {
        async getAllSupplierGroup({ commit }) {
            const result = await supplierGroupService.getAllSupplierGroup()
            if(result){
                commit('setSupplierGroup', result)
                return result
            }
            return null
        },
        async getSupplierGroups({ commit },data) {
            const result = await supplierGroupService.getSupplierGroups(data.page, data.pageSize)
            if(result){
                commit('setSupplierGroup', result)
                return result
            }
            return null
        }
    },
    getters: {
        supplierGroup: (state) => {
            return state.supplierGroup
        },
    },
    mutations: {
        setSupplierGroup: (state, supplierGroup) => {
            state.supplierGroup = supplierGroup
        },
    }
}