import saleOrder from '../services/sale-order.service';

export default {
    namespaced: true,
    state: {
        priceTotal: 0,
        editOrderItems: [],
        saleOrders: [],
        saleOrdersCreate: {},
        pagination: 0,
        tags: [],
        saleOrder: {},
        page: {}
    },
    actions: {
        async getSaleOrder({ commit, state, dispatch, getters }, data) {
            state.page = data
            const result = await saleOrder.getSaleOrder(data.page, data.pageSize)
            if (result) {
                var listSaleOrder = result.items
                for (let i = 0; i < listSaleOrder.length; i++) {
                    dispatch('getOrderItems', listSaleOrder[i].id).then(() => {
                        var rs = state.editOrderItems
                        var r = listSaleOrder[i]
                        listSaleOrder[i] = Object.assign(r, { priceTotal: getters['getPriceTotal'], orderItems: rs, paymentType: r.payment_type })
                    })
                }
                commit('setPagination', result.pagination.totalElements)
                commit('setSaleOrder', listSaleOrder)
                return listSaleOrder
            }
            return null
        },
        async getSaleOrderDetail({ commit }, data) {
            const result = await saleOrder.getSaleOrderDetail(data)
            if (result) {
                commit('setOrderDetail', result)
                commit('setTags', result.tags)
                return result
            }
        },
        async deleteSaleOrder({ dispatch, state }, data) {
            const result = await saleOrder.deleteSaleOrder(data)
            if (result) {
                dispatch('getSaleOrder', state.page)
                return result
            }
            return null
        },
        async createSaleOrder({ dispatch, state }, data) {
            const result = await saleOrder.createSaleOrder(data)
            if (result) {
                dispatch('getSaleOrder', state.page)
                return result
            }
            return null
        },
        async updateSaleOrder({ dispatch, state }, data) {
            const result = await saleOrder.updateSaleOrder(data)
            if (result) {
                dispatch('getSaleOrder', state.page)
                return result
            }
            return null
        },
        async getOrderItems({ commit }, data) {
            const result = await saleOrder.getOrderItems(data)
            if (result) {
                commit('setOrderItems', result)
                return result
            }
            return null
        },
        async updateItem({ commit }, data) {
            const result = await saleOrder.updateItem(data.id, data.dataItem)
            if (result) {
                commit('getOrderItems', result)
                return result
            }
            return null
        },
        async addItem({ commit }, data) {
            const result = await saleOrder.addItem(data.id, data.dataItem)
            if (result) {
                commit('getOrderItems', result)
                return result
            }
            return null
        },
        async deleteItem({ commit }, data) {
            const result = await saleOrder.deleteItem(data.id, data.vid)
            if (result) {
                commit('getOrderItems', result)
                return result
            }
            return null
        },
        async getSaleOrderById({ state }, data) {
            return await state.saleOrders.find(r => r.id === data.id)
        },
    },
    getters: {
        getSaleOrder: (state) => {
            return state.saleOrders
        },
        getSaleOrderDetail: (state) => {
            return state.saleOrder
        },
        getSaleCreated: (state) => {
            return state.saleOrdersCreate
        },
        getEditOrderItems: (state) => {
            return state.editOrderItems
        },
        getPriceTotal: (state) => {
            return state.priceTotal
        },
        getPagination: (state) => {
            return state.pagination
        },
        getTags: (state) => {
            return state.tags
        },
    },
    mutations: {
        setSaleOrder: (state, saleOrders) => {
            state.saleOrders = saleOrders

        },
        setOrderDetail: (state, saleOrder) => {
            state.saleOrder = saleOrder

        },
        setOrderItems: (state, orderItems) => {
            var rs = orderItems.map(r => ({ ...r, name: r.product_name, unit: r.unit_name, prices: ((r.number * r.price) * (1 + r.tax / 100) -(r.number * r.price) * (r.discount/100)) > 0 ? (r.number * r.price) * (1 + r.tax / 100) - r.discount : 0, sku: r.barcode }))
            // var rs = state.listVariant.find(r=> r.variant_id===orderItems.variant_id)
            var total = 0
            rs.map(r => { total += r.prices })
            state.editOrderItems = rs
            state.priceTotal = total
        },
        setPagination: (state, data) => {
            state.pagination = data
        },
        setTags: (state, data) => {
            state.tags = data
        },
    }
}