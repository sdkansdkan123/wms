import brandService from '../services/brand.service';
export default {
    namespaced: true,
    state: {
        brands: [],
        brand: {
            image: []
        },
        page:{},
        totalElements:null
    },
    actions: {
        async getBrands({ commit ,state}, data) {
            state.page=data
            const result = await brandService.getBrands(data.page, data.pageSize)
            if (result) {
                commit('setBrands', result)
                return result
            }
            return null
        },
        async updateBrand({ commit }, data) {
            const result = await brandService.updateBrand(data)
            if (result) {
                commit('updateBrand', data)
                return result
            }
            return null
        },
        async deleteBrand({ commit }, data) {
            const result = await brandService.deleteBrand(data)
            if (result) {
                commit('deleteBrand', data)
                return result
            }
            return null
        },
        async addBrand({ dispatch, state }, data) {
            const result = await brandService.addBrand(data)

            if (result) {

                dispatch('getBrands', state.page)
                return result
            }
            return null
        },
        async brandDetailUpdate({ commit }, data) {
            const result = await brandService.brandDetail(data)
            if (result) {
                result.image = result.image ? [{url: result.image}] : []
                await commit('setBrandUpdate', result)
                return result
            }
            return null
        },
        async brandDetail({ commit }, data) {
            commit('setBrand', data)
            return null
        }
    },
    getters: {
        getBrands: (state) => {
            return state.brands
        },
        getBrand: (state) => {
            return state.brand
        },
        getTotalElements: (state) => {
            return state.totalElements
        }
    },
    mutations: {
        setBrands: (state, brands) => {
            state.brands = brands.items
            state.totalElements=brands.pagination.totalElements
        },
        setBrandUpdate: (state, brand) => {
            state.brand = brand
            state.brand.isUpdate = true
        },
        setBrand: (state, page) => {
            state.brand = {
                title: '',
                image: [],
                active: 0,
                isUpdate: false,
                page: page
            }
        },
        updateBrand: (state, brand) => {
            state.brands = state.brands.map(r => {
                if (r.id === brand.id) {
                    return brand
                }
                else {
                    return r
                }

            })
        },
        deleteBrand: (state, brand) => {
            state.brands = state.brands.filter(r => r.id !== brand)
        },
    }
}