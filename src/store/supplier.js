import supplierService from '../services/supplier.service';


export default {
    namespaced: true,
    state: {
        supplier: [],
        suppliers:[],
        address:[],
        allSupplier:[],
        currentSupplier:[]
    },
    actions: {
        async getAllSuppliers({ commit }) {
            const result = await supplierService.getSupplier()
            if(result){
                commit('setAllSuppliers', result)
                return result
            }
            return null
        },
        async getSuppliers({ commit },data) {
            const result = await supplierService.getSuppliers(data.page, data.pageSize)
            if(result){
                commit('setSuppliers', result)
                return result
            }
            return null
        },
        async addSupplier({ commit }, data) {
            const result = await supplierService.addSupplier(data)
            if(result){
                commit('setCurrentSupplier', data)
                return result
            }
            return null
        },
        async updateSupplier({commit}, data){
            const result = await supplierService.updateSupplier(data)
            if(result){
                commit('setCurrentSupplier', data)
                return result
            }
            return null
        },
        async getListAllSupplierAddress({commit}, data){
            const result = await supplierService.getListAllSupplierAddress(data)
            if(result){
                commit('setAddress', result)
                return result
            }
            return null
        },
        async getSupplierAddress({commit}, data){
            const result = await supplierService.getSupplierAddress(data.id,data.page,data.pageSize)
            if(result){
                commit('setAddress', result)
                return result
            }
            return null
        },
        setAddress({commit}, data){
            commit('setAddress', data)
        }
    },
    getters: {
        supplier: (state) => {
            return state.supplier
        },
        address: (state) => {
            return state.address
        },
        allSupplier: (state) => {
            return state.allSupplier
        },
        suppliers: (state) =>{
            return state.suppliers
        }
    },
    mutations: {
        setSupplier: (state, supplier) => {
            state.supplier = supplier
        },
        setAllSuppliers: (state, supplier) => {
            state.allSupplier = supplier
        },
        setAddress: (state, address) => {
            state.address = address
        },
        setCurrentSupplier:(state, supplier) => {
            state.currentSupplier = supplier
        },
        setSuppliers:(state, supplier) => {
            state.suppliers = supplier
        },
    }
}