import transferService from '../services/transfer.service';

export default {
    namespaced: true,
    state: {
        allTransfers:[],
        transfers:[],
        items:[],
        transfer:{}
    },
    actions: {
        async getAllTransfer({ commit }) {
            const result = await transferService.getAllTransfer()
            if(result){
                commit('setAllTransfers', result)
                return result
            }
            return null
        },
        async getTransfers({ commit }, data) {
            const result = await transferService.getTransfers(data.page, data.pageSize)
            if(result){
                commit('setTransfers', result)
                return result
            }
            return null
        },
        async getAllItem({ commit }, transfer_id) {
            const result = await transferService.getAllItem(transfer_id)
            if(result){
                commit('setItems', result)
                return result
            }
            return null
        },
        async getItems({ commit }, data) {
            const result = await transferService.getItems(data.id, data.page, data.pageSize)
            if(result){
                commit('setItems', result)
                return result
            }
            return null
        },
        async addTransfer({ commit }, data) {
            const result = await transferService.addTransfer(data)
            if(result){
                commit('setTransfer', data)
                return result
            }
            return null
        },
        async updateTransfer({ commit }, data) {
            const result = await transferService.updateTransfer(data)
            if(result){
                commit('setTransfer',data)
                return result
            }
            return null
        },
        async updateItem({ commit }, data) {
            const result = await transferService.updateItem(data.id,data.data)
            if(result){
                commit('')
                return result
            }
            return null
        },
        async deleteItem({ commit }, data) {
            const result = await transferService.deleteItem(data.id,data.vid)
            if(result){
                commit('')
                return result
            }
            return null
        },
        async addItem({ commit }, data) {
            const result = await transferService.addItem(data.id,data.data)
            if(result){
                commit('')
                return result
            }
            return null
        },
    },
    getters: {
        allTransfers: (state) => {
            return state.allTransfers
        },
        transfers: (state) => {
            return state.transfers
        },
        items:(state) => {
            return state.items
        }
    },
    mutations: {
        setAllTransfers: (state, allTransfers) => {
            state.allTransfers = allTransfers
        },
        setTransfers: (state, transfers) => {
            state.transfers = transfers
        },
        setItems: (state, items) => {
            state.items = items
        },
        setTransfer: (state, transfer) => {
            state.transfer = transfer
        },
    }
}