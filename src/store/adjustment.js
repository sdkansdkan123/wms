import adjustmentService from '../services/adjustment.service';

export default {
    namespaced: true,
    state: {
        adjustments: [],
        adjustment: {},
        dialogQueue: [],
        groupAddEdit: {
            data: {},
            isUpdate: 'false'
        }
    },
    actions: {
        async loadAdjustments({ commit }, data) {
            const result = await adjustmentService.getAdjustments(data.page, data.pageSize)
            if(result){
                commit('setAdjustments', result)
                return result
            }
            return null
        },
        async addAdjustment({commit}, data){
            const result = await adjustmentService.addAdjustment(data)
            if(result){
                commit('setAdjustment', result)
                return result
            }
            return null
        },
        showDialog({commit}, dialogName){
            commit('pushToDialogQueue', dialogName);
        },
        hideDialog({commit}){
            commit('popDialogFromQueue')
        },
        onUpdateAdjustment({commit},data){
            commit('setUpdateAdjustment',data)
        },
    },
    getters: {
        getAdjustments: (state) => {
            return state.adjustments
        },
        getAdjustment: (state) => {
            return state.adjustment
        },
        currentDialog(state){
            return state.dialogQueue[state.dialogQueue.length - 1];
        },
        getGroupAddEdit:(state)=>{
            return  state.groupAddEdit;
        }
    },
    mutations: {
        setAdjustments: (state, adjustments) => {
            state.adjustments = adjustments
        },
        setAdjustment: (state, adjustment) => {
            state.adjustment = adjustment
        },
        setUpdateAdjustment:(state, data)=>{
            state.adjustment = data
        },
        pushToDialogQueue(state, name){
            const exist = state.dialogQueue.find(d => d === name);
            if(!exist){
                state.dialogQueue = [...state.dialogQueue, name];
            }
        },
        popDialogFromQueue(state){
            if(state.dialogQueue.length > 0){
                state.dialogQueue.pop();
            }
        }
    }
}