import returnOrderService from '../services/return-order.service';

export default {
    namespaced: true,
    state: {
        returnOrder:[],
        returnOrderAddEdit:{},
        items:{},
        currentReturnOrder:[],
        currentSaleOrder:[],
        itemDeleteId:null,
    },
    actions: {
        async getReturnOrder({commit}, data) {
            const result = await returnOrderService.getReturnOrder(data.page, data.pageSize)
            if(result){
                commit('setReturnOrder', result)
                return result
            }
            return null
        },
        async addReturnOrder({commit}, data) {
            const result = await returnOrderService.addReturnOrder(data)
            if(result){
                commit('setReturnOrderAddEdit', data)
                return result
            }
            return null
        },
        async updateReturnOrder({commit}, data) {
            const result = await returnOrderService.updateReturnOrder(data)
            if(result){
                commit('setReturnOrderAddEdit', data)
                return result
            }
            return null
        },
        async getItemReturnOrders({commit}, data) {
            const result = await returnOrderService.getItemReturnOrders(data.id, data.page, data.pageSize)
            if(result){
                commit('setItems', result)
                return result
            }
            return null
        },
        async getItemSaleOrders({commit}, data) {
            const result = await returnOrderService.getItemSaleOrders(data.id, data.page, data.pageSize)
            if(result){
                commit('setItems', result)
                return result
            }
            return null
        },
        async getReturnOrderDetails({commit}, id) {
            const result = await returnOrderService.getReturnOrderDetails(id)
            if(result){
                commit('setCurrentReturnOrder', result)
                return result
            }
            return null
        },
        async getSaleOrderDetails({commit}, id) {
            const result = await returnOrderService.getSaleOrderDetails(id)
            if(result){
                commit('setCurrentSaleOrder', result)
                return result
            }
            return null
        },
        async deleteReturnOrderItems({commit}, data) {
            const result = await returnOrderService.deleteReturnOrderItems(data.id, data.vid)
            if(result){
                commit('setItemDeleteId', result)
                return result
            }
            return null
        },
        setItems({commit}, data){
            commit('setItems', data)
        }
    },
    getters: {
        returnOrder: (state) => {
            return state.returnOrder
        },
        items: (state) => {
            return state.items
        },
        currentReturnOrder: (state) => {
            return state.currentReturnOrder
        },
        currentSaleOrder: (state) => {
            return state.currentSaleOrder
        },
    },
    mutations: {

        setReturnOrder: (state, returnOrder) => {
            state.returnOrder = returnOrder

        },
        setReturnOrderAddEdit: (state, returnOrderAddEdit) => {
            state.returnOrderAddEdit = returnOrderAddEdit

        },
        setItems: (state, items) => {
            state.items = items

        },
        setCurrentReturnOrder: (state, currentReturnOrder) => {
            state.currentReturnOrder = currentReturnOrder

        },
        setCurrentSaleOrder: (state, currentSaleOrder) => {
            state.currentSaleOrder = currentSaleOrder

        },
        setItemDeleteId: (state, itemDeleteId) => {
            state.itemDeleteId = itemDeleteId

        },
    }
}