import purchaseService from '../services/purchase.service';


export default {
    namespaced: true,
    state: {
        purchaseList: [],
        purchaseByPage :[],
        purchaseDetail:{},
        orderItemList: [],

    },
    actions: {

        // Purchase
        async getPurchases({ commit }) {
            const result = await purchaseService.getPurchases()
            if(result){
                commit('setPurchase', result)
                return result
            }
            return null
        },
        async getPurchaseListByPage({ commit }, data) {
            const result = await purchaseService.getPurchaseListByPage(data.page, data.pageSize)
            if(result){
                commit('setPurchaseByPage', result)
                return result
            }
            return null
        },
        async getPurchaseDetail({ commit },id){
            const result = await purchaseService.getPurchaseDetail(id)
            if(result){
                commit('setPurchaseDetail', result)
                return result
            }
            return null
        },
        async addPurchase({dispatch}, data){
            const result = await purchaseService.addPurchase(data.values)
            if(result){
                dispatch('getPurchaseListByPage', {page:data.page, pageSize:data.pageSize})
                return result
            }
            return null
        },
        async updatePurchase({dispatch}, data){
            const result = await purchaseService.updatePurchase(data.values)
            if(result){
                dispatch('getPurchaseListByPage', {page:data.page, pageSize:data.pageSize})
                return result
            }
            return null
        },
        async deletePurchase({dispatch},data) {
            const result = await purchaseService.deletePurchase(data.id)
            if (result) {
                dispatch('getPurchaseListByPage', {page:data.page, pageSize:data.pageSize})
                return result
            }
            return null
        },
        async approvedPurchase({dispatch},data) {
            const result = await purchaseService.approvedPurchase(data.id)
            if (result) {
                dispatch('getPurchaseListByPage', {page:data.page, pageSize:data.pageSize})
                return result
            }
            return null
        },
       //Order Item
       async getOrderItemList({commit}, id){
        const result = await purchaseService.getOrderItemList(id)
        if(result){
            commit('setOrderItem', result)
            return result
        }
        return null
        },
        async addOrderItem( {dispatch},item){
            const result = await purchaseService.addOrderItem(item.id, item.data)
            if(result){
                dispatch('getPurchaseListByPage', {page:item.page, pageSize:item.pageSize})
                return result
            }
            return null
        },
        async updateOrderItem( {dispatch},item){
            const result = await purchaseService.updateOrderItem(item.id, item.data)
            if(result){
                dispatch('getPurchaseListByPage', {page:item.page, pageSize:item.pageSize})
                return result
            }
            return null
        },
        async deleteOrderItem({dispatch},item) {
            const result = await purchaseService.deleteOrderItem(item.id, item.vid)
            if (result) {
                dispatch('getOrderItemList', item.id)
                return result
            }
            return null
        },
        
    },
    getters: {
        purchases: (state) => {
            return state.purchaseList
        },
        purchaseDetail: (state) => {
            return state.purchaseDetail
        },
        orderItem: (state) => {
            return state.orderItemList
        },
        PurchaseByPage: (state) => {
            return state.purchaseByPage
        }
       
    },
    mutations: {
        setPurchase: (state, purchase) => {
            state.purchaseList = purchase
        },
        setPurchaseByPage: (state, purchase) => {
            state.purchaseByPage = purchase
        },
        setPurchaseDetail: (state, purchaseDetail) =>{
            state.purchaseDetail = purchaseDetail
        },
        setOrderItem: (state, orderItems) => {
            state.orderItemList = orderItems
        },

    }
}