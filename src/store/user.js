import userService from '../services/user.service';

export default {
    namespaced: true,
    state: {
        users: [],
        userCreated: {},
        roleGroupOfUser:[],
        roleGroupAdd:[],
        roleGroupDel:[]
    },
    actions: {
        async loadUsers({ commit }, data) {
            console.log('action load user')
            const result = await userService.getUsers(data.page, data.pageSize)
            if(result){
                commit('setUsers', result)
                return result
            }
            return null
        },
        async addUser({ commit }, data){
            const result = await userService.createUser(data)
            if(result){
                commit('setUser', result)
                return result
            }
            return null
        },
        async updateUser({ commit }, data){
            const result = await userService.updateUser(data)
            if(result){
                commit('setUser', result)
                return result
            }
            return null
        },
        async deleteUser({ commit }, id){
            const result = await userService.deleteUser(id)
            if(result){
                commit('setUser', result)
                return result
            }
            return null
        },
        async addToGroup({commit}, {id,role_list}){ 
            console.log('addToGroup action', {id,role_list});
            const data = {role_list: role_list}
            const result = await userService.addToGroup(id,data)//ok a
            commit('setRoleGroupAdd',role_list);
            if(result){
                
                return result
            }
            return null
        },
        async delFromGroup({commit}, {id,role_list}){ 
            console.log('delToGroup action', {id,role_list});
            const data = {role_list: role_list}
            const result = await userService.delFromGroup(id,data)//ok a
            commit('setRoleGroupDel',role_list);
            if(result){
                
                return result
            }
            return null
        },
        async getRoleGroupOfUser({commit},id){
            const result = await userService.getRoleGroup(id)//ok a
            commit('setRoleGroupUser', result);
            if(result){
                
                return result
            }
            return null
        }
    },
    getters: {
        getUsers: (state) => {
            return state.users
        },
        getUser: (state) => {
            return state.usersCreated
        }
    },
    mutations: {
        setUsers: (state, users) => {
            state.users = users
        },
        setUser: (state, userCreated) => {
            state.userCreated = userCreated
        },
        setRoleGroupUser: (state, roleGroupOfUser) => {
            state.roleGroupOfUser = roleGroupOfUser
        },
        setRoleGroupAdd: (state, roleGroupAdd) =>{
            state.roleGroupAdd = roleGroupAdd
        },
        setRoleGroupDel:(state, roleGroupDel) =>{
            state.roleGroupDel = roleGroupDel
        }
    }
}