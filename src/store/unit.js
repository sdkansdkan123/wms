import unitService from '../services/unit.service';

export default {
    namespaced: true,
    state: {
        units: [],
        baseUnits: []
    },
    actions: {
        async loadUnits({ commit }, data) {
            const result = await unitService.getUnits(data.page, data.pageSize)
            if(result){
                commit('setUnits', result)
                return result
            }
            return null
        },
        async loadBaseUnits({ commit }, data) {
            const rs = await unitService.getUnits(data.page, data.pageSize)

            if(rs) {
                commit('setBaseUnits', rs.items)
                return rs
            }

            return null
        },
        async addUnit({dispatch}, data) {
            const rs = await unitService.addUnit(data)

            if(rs){
                dispatch('loadUnits', {page:0, pageSize:100})
                return rs
            }

            return null
        },
        async updateUnit({ commit }, data) {
            const rs = await unitService.updateUnit(data)

            if(rs) {
                commit('updateUnitSuccess', data)
                return rs
            }

            return null
        },
        async deleteUnit({ commit }, id) {
            const rs = await unitService.deleteUnit(id)

            if(rs) {
                commit('deleteUnitSuccess', id)
                return rs
            }

            return null
        }
    },
    getters: {
        getUnits: (state) => {
            return state.units
        },
        getBaseUnits: (state) => {
            return state.baseUnits
        }
    },
    mutations: {
        setUnits: (state, units) => {
            state.units = units
        },
        setBaseUnits: (state, baseUnits) => {
            state.baseUnits = baseUnits
        },
        updateUnitSuccess: (state, unit) => {
            state.units.items = state.units.items.map(r => {
                if (r.id === unit.id) {
                    return unit
                }
                else {
                    return r
                }
            })  
        },
        deleteUnitSuccess: (state, id) => {
            state.units.items =  state.units.items.filter(a => a.id !== id)
        }
    }
}