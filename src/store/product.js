import productService from '../services/product.service';

export default {
    namespaced: true,
    state: {
        products: [],
        allProducts: [],
        product: {},
        delProduct: {},
        variants: [],
        warehouses: [],
        units: {},
        other: {},
        price: {}
    },
    actions: {
        async loadProducts({ commit }, data) {
            const result = await productService.getProducts(data.page, data.pageSize, data.type)
            if(result){
                commit('setProducts', result)
                return result
            }
            return null
        },
        async loadAllProducts({ commit }) {
            const result = await productService.getAllProducts()
            if(result){
                commit('setAllProducts', result)
                return result
            }
            return null
        },
        async loadProductById({commit}, id){
            const result = await productService.getProductById(id)
            if(result){
                commit('setProduct', result)
                return result
            }
            return null
        },
        async addProduct({dispatch}, data){
            const result = await productService.addProduct(data)
            if(result){
                dispatch('loadProducts', {page: 0, pageSize: 10, type: "PRODUCT"});
                return result
            }
            return null
        },
        async updateProduct({dispatch}, data){
            const result = await productService.updateProduct(data)
            if(result){
                dispatch('loadProducts', {page: 0, pageSize: 10, type: "PRODUCT"});
                return result
            }
            return null
        },
        async deleteProduct({dispatch}, id){
            const result = await productService.deleteProduct(id)
            if(result){
                dispatch('loadProducts', {page: 0, pageSize: 10, type: "PRODUCT"});
                return result
            }
            return null
        },
        async loadVariants({commit}, id){
            const result = await productService.getVariants(id)
            if(result){
                commit('setVariants', result)
                return result
            }
            return null
        },
        async loadUnits({commit}, id){
            const result = await productService.getUnits(id)
            if(result){
                commit('setUnits', result)
                return result
            }
            return null
        },
        async loadWarehouses({commit}, id){
            const result = await productService.getWarehouses(id)
            if(result){
                commit('setWarehouses', result)
                return result
            }
            return null
        },
        async loadOthers({commit}, id){
            const result = await productService.getOthers(id)
            if(result){
                commit('setOthers', result)
                return result
            }
            return null
        },
        async updateVariant({dispatch}, data){
            const result = await productService.putVariant(data.id, data.data)
            if(result){
                dispatch('loadVariants', data.id)
                return result
            }
            return null
        },
        async updateUnit({dispatch}, data){
            const result = await productService.putUnit(data.id, data.data)
            if(result){
                dispatch('loadUnits', data.id)
                return result
            }
            return null
        },
        async updateWarehouse({dispatch}, data){
            const result = await productService.putWarehouse(data.id, data.data)
            if(result){
                dispatch('loadWarehouses', data.id)
                return result
            }
            return null
        },
        async updateOther({dispatch}, data){
            const result = await productService.putOther(data.id, data.data)
            if(result){
                dispatch('loadOthers', data.id)
                return result
            }
            return null
        },
        async updatePrice({dispatch}, data){
            const result = await productService.putPrice(data.id, data.data)
            if(result){
                dispatch('loadProducts', {page: 0, pageSize: 10, type: "PRODUCT"})
                return result
            }
            return null
        },
    },
    getters: {
        getProducts: (state) => {
            return state.products
        },
        getAllProducts: (state) => {
            return state.allProducts
        },
        getProduct: (state) => {
            return state.product
        },
        getVariants: (state) => {
            return state.variants
        },
        getUnits: (state) => {
            return state.units
        },
        getWarehouses: (state) => {
            return state.warehouses
        },
        getOthers: (state) => {
            return state.others
        }
    },
    mutations: {
        setProducts: (state, products) => {
            state.products = products
        },
        setAllProducts: (state, allProducts) => {
            state.allProducts = allProducts
        },
        setProduct: (state, product) => {
            state.product = product
        },
        setUpdateProduct:(state, data)=>{
            state.product = data
        },
        setDeleteProduct: (state, data)=>{
            state.delProduct = data
        },
        setVariants: (state, data) => {
            state.variants = data
        },
        setUnits: (state, data) => {
            state.units = data
        },
        setWarehouses: (state, data) => {
            state.warehouses = data
        },
        setOthers: (state, data) => {
            state.others = data
        },
    }
}