import CustomerMgmtService from '@/services/customer-mgmt.service';

export default{
    namespaced: true,
    state: {
        customerList: [],
        customerGroupList: [],
        customerGroupListAll: [],
        customerContacts: [],
        count: 0,
        customerAddresses: [],
        temporaryCustomerContacts: [],
        temporaryCustomerAddresses: []
    },
    actions:{

        // Get data from database
        async getCustomerList({commit}, data){
            
            const result = await CustomerMgmtService.getCustomerList(data.page, data.pageSize)
     
            if(result){
                commit('setCustomerList', result.data.data)
                return result.data.data
            }
            return null
        },
        async getCustomerGroupListAll({commit}){
            var result = result = await CustomerMgmtService.getCustomerGroupList(null, null);
    
            if(result){
                commit('setCustomerGroupListAll',result.data.data.items)
                return result.data.data.items
               
            }
            return null

        },
        async getCustomerGroupList({commit}, data){
            var result = result = await CustomerMgmtService.getCustomerGroupList(data.page, data.pageSize);
             
    
            if(result){
                commit('setCustomerGroupList',result.data.data)
                return result.data.data
            }
            return null
        },
        async getCustomerContacts({commit},id){
            const result = await CustomerMgmtService.getCustomerContacts(id)
  
            if(result){
                commit('setCustomerContacts',result.data.data.items)
                return result.data.data.items
            }
            return null
        },
        async getCustomerAddresses({commit}, cid){
            const rs = await CustomerMgmtService.getCustomerAddresses(cid)

            if(rs){
                commit('setCustomerAddresses',rs.data.data.items)
                return rs.data.data.items
            }
            return null
        },

        //post
        async addCustomerGroup({dispatch},data) {
            const result = await CustomerMgmtService.addCustomerGroup(data.data)

            if (result) {
                if(data.page != null && data.pageSize != null){
                    dispatch('getCustomerGroupList',{page: data.page, pageSize: data.pageSize})
                }
                return result
            }
            return null
        },
        async addCustomer(data) {
            const result = await CustomerMgmtService.addCustomer(data)
            
            if (result) {
                // commit('addCustomerSuccess',data)
                // dispatch('getCustomerList')
                return result
            }
            return null
        },
        async addCustomerContact({commit}, contact) {
            const result = await CustomerMgmtService.addCustomerContact(contact.id,contact.data)

            if (result) {
                commit('addCustomerContactSuccess', contact.data)
                return result
            }

            return null
        },
        async addCustomerAddress({commit}, address) {
            const result = await CustomerMgmtService.addCustomerAddress(address.id,address.data)

            if (result) {
                commit('addCustomerAddressSuccess', address.data)
                return result
            }

            return null
        },
        
        //put
        async updateCustomerGroup({ dispatch},data) {
            const result = await CustomerMgmtService.updateCustomerGroup(data.data)
            if (result) {
                if(data.page != null && data.pageSize != null){
                    dispatch('getCustomerGroupList',{page: data.page, pageSize: data.pageSize})
                }
                return result
            }
            return null
        },
        async updateCustomer({commit},data) {
            const result = await CustomerMgmtService.updateCustomer(data)

            if (result) {
                commit('updateCustomerSuccess',data)
                return result
            }
            return null
        },
        async updateCustomerContact({commit},contact) {
            const result = await CustomerMgmtService.updateCustomerContact(contact.id,contact.data)
            if (result) {
                commit('updateCustomerContactSuccess', contact.data)
                return result
            }
            return null
        },
        async updateCustomerAddress({commit},address) {
            const result = await CustomerMgmtService.updateCustomerAddress(address.id,address.data)
            if (result) {
                commit('updateCustomerAddressSuccess', address.data)
                return result
            }
            return null
        },

        //delete
        async deleteCustomerGroup({dispatch},data) {
            const result = await CustomerMgmtService.deleteCustomerGroup(data.id)
            if (result) {
                if(data.page != null && data.pageSize != null){
                    dispatch('getCustomerGroupList',{page: data.page, pageSize: data.pageSize})
                }
                // commit('deleteCustomerGroupSuccess', id)
                return result
            }
            return null
        },
        async deleteCustomer({commit},id) {
            const result = await CustomerMgmtService.deleteCustomer(id)
            if (result) {
                commit('deleteCustomerSuccess',id)
                return result
            }
            return null
        },
        async deleteCustomerContact({commit}, contact) {
            const result = await CustomerMgmtService.deleteCustomerContact(contact.cid, contact.id)
            if (result) {
                commit('deleteCustomerContactSuccess', contact.id)   
                return result
            }
            
            return null
        },
        async deleteCustomerAddress({commit}, address) {
            const result = await CustomerMgmtService.deleteCustomerAddress(address.cid, address.id)
            if (result) {
                commit('deleteCustomerAddressSuccess', address.id)   
                return result
            }
            
            return null
        },

        // Temporary Customer Data
        resetTemporaryCustomerContacts({commit}){
            commit('resetTemporaryCustomerContacts')
            return null
        },
        resetTemporaryCustomerAddresses({commit}){
            commit('resetTemporaryCustomerAddresses')
            return null
        },
        async addTemporaryCustomerContact({commit}, contact) {
            commit('addTemporaryCustomerContactSuccess', contact)
            return contact
        },
        async addTemporaryCustomerAddress({commit}, address) {
            commit('addTemporaryCustomerAddressSuccess', address)
            return address
        },        
        async updateTemporaryCustomerContact({commit},contact) {
            commit('updateTemporaryCustomerContactSuccess', contact)           
            return null
        },
        async updateTemporaryCustomerAddress({commit},address) {
            commit('updateTemporaryCustomerAddressSuccess', address)           
            return null
        },
        async deleteTemporaryCustomerContact({commit}, id){
            commit('deleteTemporaryCustomerContactSuccess', id)           
            return null
        },
        async deleteTemporaryCustomerAddress({commit}, id){
            commit('deleteTemporaryCustomerAddressSuccess', id)           
            return null
        }
    },
    getters:{
        customerList:(state) => {
            return state.customerList
        },
        customerGroupList:(state) => {
            return state.customerGroupList
        },
        customerGroupListAll:(state) => {
            return state.customerGroupListAll
        },
        customerContacts: (state) => {
            return state.customerContacts
        },
        customerAddresses: (state) => {
            return state.customerAddresses
        },
        temporaryCustomerContacts: (state) =>{
            return state.temporaryCustomerContacts
        },
        temporaryCustomerAddresses: (state) => {
            return state.temporaryCustomerAddresses
        }
    },
    mutations:{
        setCustomerList: (state, customerList) => {
            state.customerList = customerList
        },
        setCustomerGroupList: (state, customerGroupList) => {
            state.customerGroupList = customerGroupList
        },
        setCustomerGroupListAll: (state, customerGroupListAll) => {
            state.customerGroupListAll = customerGroupListAll
        },
        setCustomerContacts: (state, customerContacts) => {
            state.customerContacts = customerContacts
        },
        setCustomerAddresses: (state, addresses) => {
            state.customerAddresses = addresses
        },

        // Customer
        addCustomerSuccess: (state, customer) => {
            state.customerList = [...state.customerList, customer]
        },
        updateCustomerSuccess: (state, customer) => {
            state.customerList = state.customerList.map(r => { //ERROR
                if(r.id === customer.id){
                    return customer
                }
                else{
                    return r;
                }
            })    
        },
        deleteCustomerSuccess: (state, id) => {
            state.customerList =  state.customerList.filter(c => c.id !== id)
        },

        //CustomerGroup
        // addCustomerGroupSuccess: (state, data) => {
        //     // if(data.page && data.pageSize)
        //     // state.customerGroupList = [...state.customerGroupList, customerGroup]
        // },
        updateCustomerGroupSuccess:(state, customerGroup) => {
            state.customerGroupList = state.customerGroupList.map(r => {
                if(r.id === customerGroup.id){
                    return customerGroup
                }
                else{
                    return r;
                }
            })   
        },
        deleteCustomerGroupSuccess: (state, id) => {
            state.customerGroupList =  state.customerGroupList.filter(c => c.id !== id)
        },

        //CustomerContact
        addCustomerContactSuccess: (state, contact) => {
            state.customerContacts = [...state.customerContacts, contact]
        },
        deleteCustomerContactSuccess: (state, id) => {
            state.customerContacts =  state.customerContacts.filter(r => r.id !== id)
        },
        updateCustomerContactSuccess: (state, contact) => {
            state.customerContacts = state.customerContacts.map(r => {
                if(r.id === contact.id){
                    return contact
                }
                else{
                    return r;
                }
            })            
        },

        //CustomerAddress
        addCustomerAddressSuccess: (state, address) => {
            state.customerAddresses = [...state.customerAddresses, address]
        },
        deleteCustomerAddressSuccess: (state, id) => {
            state.customerAddresses =  state.customerAddresses.filter(r => r.id !== id)
        },
        updateCustomerAddressSuccess: (state, address) => {
            state.customerAddresses = state.customerAddresses.map(r => {
                if(r.id === address.id){
                    return address
                }
                else{
                    return r;
                }
            })            
        },

        //TemporaryCustomerContact
        addTemporaryCustomerContactSuccess: (state, contact) =>{
            state.temporaryCustomerContacts = [...state.temporaryCustomerContacts, contact]
        },
        updateTemporaryCustomerContactSuccess: (state, contact) => {
            state.temporaryCustomerContacts = state.temporaryCustomerContacts.map(r => {
                if(r.id === contact.id){
                    return contact
                }
                else{
                    return r;
                }
            })            
        },
        deleteTemporaryCustomerContactSuccess: (state, id) => {
            state.temporaryCustomerContacts =  state.temporaryCustomerContacts.filter(r => r.id !== id)
        },

        // TemporaryCustomerAddress
        addTemporaryCustomerAddressSuccess: (state, address) =>{
            state.temporaryCustomerAddresses = [...state.temporaryCustomerAddresses, address]
        },
        updateTemporaryCustomerAddressSuccess: (state, address) => {
            state.temporaryCustomerAddresses = state.temporaryCustomerAddresses.map(a => {
                if(a.id === address.id){
                    return address
                }
                else{
                    return a;
                }
            })            
        },
        deleteTemporaryCustomerAddressSuccess: (state, id) => {
            state.temporaryCustomerAddresses =  state.temporaryCustomerAddresses.filter(a => a.id !== id)
        },

        resetTemporaryCustomerContacts: (state) =>{
            state.temporaryCustomerContacts = []
        },
        resetTemporaryCustomerAddresses: (state) => {
            state.temporaryCustomerAddresses = []
        }
    }
}