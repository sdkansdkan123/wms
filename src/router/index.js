import Vue from 'vue'
import VueRouter from 'vue-router'
import authGuard from '@/router/guards/auth.guard'
import forwardGuard from '@/router/guards/forward.guard'
//#region 

//Adding layouts router.
const BlankLayout = () => import("@/layouts/BlankLayout")
const Layout1 = () => import("@/layouts/backend/Layout-1")

//function element
const ProductManagement = () => import('@/views/Product/ProductManagement')
const UserManagement = () => import('@/views/User/UserManagement')
const AdjustmentManagement = () => import('@/views/Adjustment/AdjustmentManagement')

//Sale Order Element 
const OrderManagement = () => import('@/views/SaleOrder/SaleOrderManagement')
//Transfer Element 
const TransferManagement = () => import('@/views/Transfer/TransferManagement')
//Product element
const MenuManagement = () => import('@/views/Function/MenuManagement')

const CustomerManagement = () => import('@/views/CustomerManagement/CustomerManagement')
const Test = () => import('@/components/table/Test')

//Warehouse element

const WarehouseManagement = () => import('@/views/Warehouse/WarehouseManagement')
// Inventory
const InventoryManagement = () => import('@/views/Inventory/InventoryManagement')
//Purchase element
const PurchaseManagement = () => import('@/views/PurchaseManagement/PurchaseManagement')
//ReturnOrderManagement element
const ReturnOrderManagement = () => import('@/views/ReturnOrder/ReturnOrderManagement')
Vue.use(VueRouter)

//#region 

const childRoute = () => [
  // {
  //   path:'/customer-management',
  //   name: 'customer-management',
  //   component: CustomerManagement,
  // },
  // {
  //   path:'/purchase-management',
  //   name:'purchase-management',
  //   component: PurchaseManagement,
  // },
  {
    path: '/',
    component: MenuManagement
  },
  {
    path: '/user-management',
    name: 'user-management',
    component: UserManagement,
    beforeEnter: authGuard
  }
]

const functionchildRoute = () =>[
  {
    path: 'menu-management',
    name: 'menu-management',
    component: MenuManagement
  }
]
const warehousechildRoute = () =>[
  {
    path: 'warehouse-management',
    name: 'warehouse-management',
    component: WarehouseManagement
  }
]
const inventorychildRoute = () => [
  {
    path: 'inventory-management',
    name: 'inventory-management',
    component: InventoryManagement
  }
]
const productchildRoute = () => [
  {
    path: 'product-management',
    name: 'product-management',
    component: ProductManagement 
  }
]
const saleOrderchildRoute = () => [
  {
    path: 'sale-order-management',
    name: 'sale-order-management',
    component: OrderManagement 
  }
]
const PurchasechildRoute = () => [
  {
    path: 'purchase-management',
    name: 'purchase-management',
    component: PurchaseManagement 
  }
]
const CustomerchildRoute = () => [
  {
    path: 'customer-management',
    name: 'customer-management',
    component: CustomerManagement 
  }
]

const transferChildRoute = () => [
  {
    path: 'transfer-management',
    name: 'transfer-management',
    component: TransferManagement 
  }
]

const adjustmentchildRoute = () => [
  {
    path: 'adjustment-management',
    name: 'adjustment-management',
    component: AdjustmentManagement
  }
]

const returnChildRoute = () =>[
  {
    path: '',
    name: 'return-order-management',
    component: ReturnOrderManagement
  }
]
//#endregion

const routes = [
  {
    path: '/',
    name: '',
    component: Layout1,
    children: childRoute()
  },
  {
    path: '/auth',
    name: 'auth',
    component: BlankLayout,
    children: [
      {
        path: 'signup',
        name: 'signup',
        component: () =>import('@/views/Auth/SignUp')
      },
      {
        path: 'login',
        name: 'login',
        beforeEnter: forwardGuard,
        component: () =>import('@/views/Auth/Login')
      },
      {
        path: 'confirm-mail',
        name: 'auth.confirm-mail',
        component: () =>import('@/views/Auth/ForgotPassWord'),
      },
      {
        path: 'change-password',
        name: 'auth,change-password',
        component: () =>import('@/views/Auth/ForgotPassWord2'),
      }
    ]
  },
  {
    path: '/function',
    name: 'function',
    component: Layout1,
    beforeEnter : authGuard,
    children: functionchildRoute()
  },
  {
    path: '/warehouse',
    name: 'warehouse',
    component: Layout1,
    beforeEnter : authGuard,
    children: warehousechildRoute()
  },
  {
    path: '/inventory',
    name: 'inventory',
    component: Layout1,
    beforeEnter : authGuard,
    children: inventorychildRoute()
  },
  {
    path: '/product',
    name: 'product',
    component: Layout1,
    beforeEnter: authGuard,
    children: productchildRoute()
  },
  {
    path: '/adjustment',
    name: 'adjustment',
    component: Layout1,
    beforeEnter: authGuard,
    children: adjustmentchildRoute()
  },
  {
    path: '/group',
    name: 'group',
    component: Layout1,
    // children: groupchildRoute()
  },
  {
    path: '/table-list',
    name: 'Test',
    component: Test
  },
  {
    path: '/sale-order',
    name: 'sale-order',
    component: Layout1,
    beforeEnter: authGuard,
    children: saleOrderchildRoute()
  },
  {
    path: '/purchase',
    name: 'purchase',
    component: Layout1,
    beforeEnter: authGuard,
    children: PurchasechildRoute()
  },
  {
    path: '/customer',
    name: 'customer',
    component: Layout1,
    beforeEnter: authGuard,
    children: CustomerchildRoute()
  },
  {
    path: '/transfer',
    name: 'transfer',
    component: Layout1,
    beforeEnter: authGuard,
    children: transferChildRoute()
  },
  {
    path: '/return',
    name: 'return-order',
    component: Layout1,
    beforeEnter: authGuard,
    children: returnChildRoute()
  },
]


const router = new VueRouter({
  mode: 'history',
  base: process.env.VUE_APP_BASE_URL,
  routes
})

export default router
