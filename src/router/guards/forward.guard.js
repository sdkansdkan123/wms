import store from '@/store';

export default (to, from, next) => {
    var isAuthenticated = store.getters['auth/getUser']?.access_token;
   
    if(isAuthenticated) 
    {
        next({ name: 'layout.menu-management' });
    } 
    else
    {
        next();
    }
}